package Noria;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Noria.Noria;

class TestNoria {

	@Test
	void testsBasics() {
		assertEquals(42, Noria.resposta(10, 2, 30));
		assertEquals(4, Noria.resposta(20, 4, 20));
		assertEquals(342, Noria.resposta(10, 2, 180));
		assertEquals(0, Noria.resposta(20, 2, 15));
	}
	
	@Test
	void testsAltresCasos() {
		assertEquals(30, Noria.resposta(5, 30, 5));
		assertEquals(0, Noria.resposta(5, 30, 0));
		assertEquals(10, Noria.resposta(1, 1, 10));
	}
	

}

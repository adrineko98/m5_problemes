package Noria;

import java.util.Scanner;

public class Noria {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int ncasos = sc.nextInt();
		for (int i = 0; i < ncasos; i++) {
			int cabinas = sc.nextInt();
			int personas = sc.nextInt();
			int mT = sc.nextInt();
			System.out.println(resposta(cabinas,personas,mT));
		}
	}
	
	public static int resposta(int cabinas, int personas, int mT) {
		int pT = mT - (cabinas - 1);
		int f = pT * personas;
		if(f<0) {
			f=0;
		}
		return f;
	}
	
}
